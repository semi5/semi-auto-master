export function setup(ctx) {
	ctx.onCharacterSelectionLoaded(ctx => {
		// debug
		const debugLog = (...msg) => {
			mod.api.SEMI.log(`${id} v10045`, ...msg);
		};

		// script details
		const id = 'semi-auto-master';
		const title = 'SEMI Auto Master';

		// setting groups
		const SETTING_GENERAL = ctx.settings.section('General');
		const SETTING_SKILLS = ctx.settings.section('Per Skill Toggles');
		const SETTING_TOKENS = ctx.settings.section('Per Skill Mastery Token Toggles');

		// variables
		let MAX_ATTEMPTS = 3;

		// utils
		const htmlID = (id) => id.replace(/[:_]/g, '-').toLowerCase();

		// notifications
		let _notificationNuked = false;
		const _fireBottomToast = fireBottomToast;
		fireBottomToast = function (...args) { if (!_notificationNuked) { _fireBottomToast(...args); } };
		const _fireTopToast = fireTopToast;
		fireTopToast = function (...args) { if (!_notificationNuked) { _fireTopToast(...args); } };
		const _notifyPlayer = notifyPlayer;
		notifyPlayer = function (...args) { if (!_notificationNuked) { _notifyPlayer(...args); } };

		const toggleMasteryConfirm = (wasEnabled, newValue) => {
			if (wasEnabled) {
				game.settings.boolData.showMasteryCheckpointconfirmations.currentValue = newValue;
			}
		};

		// Mastery Value Cache
		let masterySkillAlt = 0;
		let masteryRealmAlt = 0;
		const masteryRealms = game.realms.allObjects;
		const masteryRealmsCap = masteryRealms.length;
		const masterySkills = game.masterySkills.filter(skill => skill.hasMastery);
		const masterySkillsCap = masterySkills.length;
		const masteryTokenXP = new Map();
		const masteryCheckpointXP = new Map();

		let _masteryTokenModifier;

		const updateMasteryTokenXP = () => {
			if (_masteryTokenModifier != game.modifiers.xpFromMasteryTokens) {
				game.masterySkills.forEach(skill => {
					skill.masteryTokens.forEach(tokens => {
						tokens.forEach(token => {
							const tokenPercent = token.percent + game.modifiers.xpFromMasteryTokens;
							const basePoolCap = skill.getBaseMasteryPoolCap(token.realm);
							const xpPerToken = Math.floor((basePoolCap * tokenPercent) / 100);

							masteryTokenXP.set(token, xpPerToken);
						})
					});
				});

				_masteryTokenModifier = game.modifiers.xpFromMasteryTokens;
			}
		}

		const updateMasteryPoolXP = () => {
			game.masterySkills.forEach(skill => {
				const realmMap = new Map();
				masteryRealms.forEach(realm => {
					const poolCap = skill.getBaseMasteryPoolCap(realm);
					const poolBonuses = skill.getMasteryPoolBonusesInRealm(realm);

					if (poolBonuses.length > 0)
						realmMap.set(realm, poolCap * ((poolBonuses[poolBonuses.length - 1].percent) / 100));
					else
						realmMap.set(realm, 0);
				});

				masteryCheckpointXP.set(skill, realmMap);
			});
		};

		// Mastery Actions
		const actionXPCost = (skill, action) => exp.level_to_xp(skill.getMasteryLevel(action) + 1) + 1 - skill.getMasteryXP(action);
		const spendTokens = (item, qty) => {
			if (qty >= 1) {
				game.bank.claimMasteryTokenOnClick(item, qty);
			}
		};

		/**
		* Get the lowest mastery action, excluding masteries equal to or above the mastery level cap.
		*/
		const lowestMastery = (skill, realm) => {
			const skillActions = skill.sortedMasteryActions;
			const masteryCap = skill.masteryLevelCap; // Normally 99

			let lowestAction = undefined;
			let lowestLevel = masteryCap;

			for (let n = 0; n < skillActions.length; n++) {
				const action = skillActions[n];

				// Wrong Realm
				if (action.realm != realm)
					continue;

				let actionMastery = skill.getMasteryLevel(action);

				if (actionMastery >= masteryCap)
					continue;

				if (actionMastery < lowestLevel) {
					lowestAction = action;
					lowestLevel = actionMastery;

					if (lowestLevel <= 1)
						break;
				}
			}

			return lowestAction;
		};

		/**
		* Check whether we should spend pool XP to upgrade a Mastery Action.
		This take into account bonus XP claimable from tokens if provided.
		*/
		const canSpendXP = (skill, action, bonusXP = 0) => {
			const realm = action.realm;

			if (!realm.isUnlocked)
				return false;

			// XP Values
			const masteryPoolXP = skill.getMasteryPoolXP(realm);
			const masteryPoolXPCap = skill.getMasteryPoolCap(realm);
			const checkpointXP = masteryCheckpointXP.get(skill).get(realm);

			// Under final checkpoint XP value.
			if (masteryPoolXP < checkpointXP)
				return false;

			// Pool is Capped
			if (masteryPoolXP >= masteryPoolXPCap && SETTING_GENERAL.get(`enable-force-spending`))
				return true;

			const xpCost = actionXPCost(skill, action);

			// No enough XP to spend.
			if (masteryPoolXP < xpCost)
				return false;

			// Spending would put under checkpoint, including bonus XP from tokens.
			if (masteryPoolXP - xpCost + bonusXP < checkpointXP)
				return false;

			return true;
		};

		/**
		* Handle a skills mastery upgrades.
		*/
		const handleSkill = (skill) => {
			for (let r = 0; r < masteryRealmsCap; r++) {
				handleSkillRealm(skill, masteryRealms[r]);
			}
		}

		const handleSkillRealm = (skill, realm) => {
			// No Mastery / No Access
			if (!skill.hasMastery || !realm.isUnlocked)
				return;

			// No Mastery to Level
			let bestMasteryAction = lowestMastery(skill, realm);
			if (!bestMasteryAction)
				return;

			// TODO: Support multiple mastery tokens per skill / realm?
			const masteryTokens = skill.masteryTokens.get(realm);
			const masteryToken = (masteryTokens && masteryTokens.length > 0) ? masteryTokens[0] : undefined;
			const poolXPCap = skill.getMasteryPoolCap(realm);
			const xpPerToken = masteryTokenXP.get(masteryToken);

			// Get Settings
			const autoMasterSkillEnabled = SETTING_SKILLS.get(`skill-${htmlID(skill.id)}`);

			if (!autoMasterSkillEnabled) {
				//debugLog('autoMasterSkillEnabled:', autoMasterSkillEnabled);
				return;
			}

			const canUseTokens = SETTING_GENERAL.get(`enable-tokens`) && masteryToken != undefined && SETTING_TOKENS.get(`token-${htmlID(masteryToken.id)}`);
			const allButOneIsEnabled = SETTING_GENERAL.get(`all-but-one`);
			const displayNotificationsSetting = game.settings.showMasteryCheckpointconfirmations;

			const tokenCount = () => {
				if (!canUseTokens)
					return 0;

				return Math.max(0, (game.bank.getQty(masteryToken) - (allButOneIsEnabled ? 1 : 0)));
			};

			const spendableTokens = (skill, realm) => {
				const tokenQty = tokenCount();
				if (tokenQty > 0) {
					const xpRemaining = poolXPCap - skill.getMasteryPoolXP(realm);
					const tokensToFillPool = Math.floor(xpRemaining / xpPerToken);
					return Math.min(tokenQty, Math.max(0, tokensToFillPool));
				}
				return 0;
			}

			// Blackhole NotificationQueue and Toastify
			_notificationNuked = true;

			// Use Tokens, check for final pool checkpoint.
			let bankTokenXP = 0;
			if (canUseTokens) {
				spendTokens(masteryToken, spendableTokens(skill, realm));
				bankTokenXP = xpPerToken * tokenCount();
			}

			// Can't afford initial mastery action.
			if (!canSpendXP(skill, bestMasteryAction, bankTokenXP)) {
				_notificationNuked = false;
				return;
			}

			// Begin Mastery Handling
			//const startTime = performance.now();
			//debugLog(`[${skill.name}] Beginning mastery spending for ${realm.name}`);

			toggleMasteryConfirm(displayNotificationsSetting, false);

			// Level Up multiple masteries.
			for (let n = 1; n <= MAX_ATTEMPTS; n++) {
				let tokenQty;
				let postSpendTokens = 0;

				if (!bestMasteryAction) {
					//debugLog(`[${skill.name}] No more mastery actions to level up, finished in ${n} loops.`);
					break;
				}

				let canAfford = canSpendXP(skill, bestMasteryAction);

				// Cannot afford, use tokens and check again.
				if (canUseTokens) {
					if (!canAfford) {
						tokenQty = spendableTokens(skill, realm);
						if (tokenQty > 0) {
							spendTokens(masteryToken, tokenQty);
							canAfford = canSpendXP(skill, bestMasteryAction);
						}
					}

					// Can't afford but would have enough tokens to restore pool.
					if (!canAfford) {
						tokenQty = tokenCount();
						if (tokenQty > 0) {
							let maxTokenXP = xpPerToken * tokenQty;
							let actionCost = actionXPCost(skill, bestMasteryAction);

							if (skill.masteryPoolXP - actionCost + maxTokenXP > masteryCheckpointXP.get(skill).get(realm)) {
								postSpendTokens = tokenQty;
								canAfford = true;
							}
						}
					}
				}

				// Still can't afford, bail.
				if (!canAfford) {
					//debugLog(`[${skill.name}] Not enough mastery XP / tokens to afford upgrade.`);
					break;
				}

				// Level Up
				skill.levelUpMasteryWithPoolXP(bestMasteryAction, 1);

				// Refill Pool
				if (postSpendTokens > 0) {
					spendTokens(masteryToken, spendableTokens(skill, realm));
				}

				// Get Next Action
				bestMasteryAction = lowestMastery(skill, realm);
			}

			// Restore NotificationQueue and Toastify
			toggleMasteryConfirm(displayNotificationsSetting, true);
			_notificationNuked = false;

			//const endTime = performance.now();
			//debugLog(`[${skill.name}] Mastery spending complete, took ${endTime - startTime}ms.`);
		};

		const handleXPAction = (skill) => {
			const autoMasterEnabled = SETTING_GENERAL.get(`semi-auto-master-enable`);
			if (!autoMasterEnabled)
				return;

			updateMasteryTokenXP();

			handleSkillRealm(skill, masteryRealms[masteryRealmAlt]);

			// Handle a looping other skill.
			handleSkillRealm(masterySkills[masterySkillAlt], masteryRealms[masteryRealmAlt]);
			masterySkillAlt++;
			if (masterySkillAlt >= masterySkillsCap) {
				masterySkillAlt = 0;
			}

			// Realm Walking
			masteryRealmAlt++;
			if (masteryRealmAlt >= masteryRealmsCap) {
				masteryRealmAlt = 0;
			}
		}

		// settings
		SETTING_GENERAL.add([{
			'type': 'switch',
			'name': `semi-auto-master-enable`,
			'label': `Enable ${title}`,
			'default': true
		}, {
			'type': 'switch',
			'name': `enable-tokens`,
			'label': `Use Mastery Tokens`,
			'default': true
		}, {
			'type': 'switch',
			'name': `all-but-one`,
			'label': `Use all but one mastery token.`,
			'default': true
		}, {
			'type': 'switch',
			'name': `enable-force-spending`,
			'label': `Force spending at 100% pool usage`,
			'hint': `This may place you under the 95% checkpoint on certain skills.`,
			'default': true
		}]);

		SETTING_SKILLS.add(
			masterySkills.map(skill => {
				return {
					'type': 'switch',
					'name': `skill-${htmlID(skill.id)}`,
					'label': $(`<span><img src="${skill.media}" class="resize-24"> Enable ${skill.name}</span>`).get(0),
					'default': true
				}
			})
		);

		const masteryTokenToggles = [];
		masterySkills.forEach(skill => {
			skill.masteryTokens.forEach(tokens => {
				tokens.forEach(token => {
					masteryTokenToggles.push({
						'type': 'switch',
						'name': `token-${htmlID(token.id)}`,
						'label': $(`<span><img src="${token.media}" class="resize-24"> Enable ${token.name}</span>`).get(0),
						'default': true
					});
				});
			});
		});
		SETTING_TOKENS.add(masteryTokenToggles);

		// game hooks
		let _masteryNotificationSetting;

		ctx.patch(NotificationQueue, 'add').replace(function (o, notification) {
			if (!_notificationNuked) {
				o(notification);
			}
		});

		ctx.onCharacterLoaded(ctx => {
			updateMasteryPoolXP();

			_masteryNotificationSetting = game.settings.showMasteryCheckpointconfirmations;
			toggleMasteryConfirm(_masteryNotificationSetting, false);

			ctx.patch(Skill, 'addXP').after(function (result, amount, action) {
				handleXPAction(this);
			});

			ctx.patch(Skill, 'addAbyssalXP').after(function (result, amount, action) {
				handleXPAction(this);
			});
		});

		ctx.onInterfaceReady(ctx => {
			toggleMasteryConfirm(_masteryNotificationSetting, true);
			MAX_ATTEMPTS = 10;
		});
	});
}
